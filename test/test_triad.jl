using AbstractPlotting

using MakieUtils

s = Scene()
scatter!(s, Point3f0[(0,0,0), (0,0,1)], show_axis=true) # need not all-zeros: https://github.com/JuliaPlots/Makie.jl/issues/800
triad!(s, 10, translation=Vec(0, 10.5, 0))
triad!(s, 5, translation=Vec(20, 0, 0), show_axis=true)
display(s)