module MakieUtils

using GLMakie
using AbstractPlotting

export triad!, titled!, vector!

"""
Draw an XYZ red/green/blue coordinate triad in the given scene
"""
function triad!(scene, len; linewidth=3, translation=Vec(0,0,0), show_axis = false)::AbstractPlotting.LineSegments
  # childscene = Scene(scene)
  ls = linesegments!(
  scene, [
  Point3f0(0) => Point3f0(len, 0, 0),
  Point3f0(0) => Point3f0(0, len, 0),
  Point3f0(0) => Point3f0(0, 0, len)
  ],
  color = [:red, :green, :blue],
  linewidth=linewidth,
  show_axis=show_axis
  ) #[end]
  translate!(ls, translation)
  return ls
end

function vector!(scene, len; linewidth=3, color=:purple)::AbstractPlotting.LineSegments
  ls = linesegments!(
    scene, [
    Point3f0(0) => Point3f0(len, 0, 0),
    ],
    color=color,
    linewidth=linewidth,
    )
    return ls
end

function titled!(scene, layout, title; labelkw=Dict())
  grid = GridLayout()
  label = grid[1,1] = Label(scene, title; labelkw...)
  # label.tellwidth = false
  # rowsize!(grid, 1, Fixed(10))
  # label.height = 10
  subl = grid[2,1] = layout
  # rowsize!(grid, 2, Fixed(50))
  # subl.tellwidth = true
  grid
end


end # module
